/**
    Copyright 2014-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.

    Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

        http://aws.amazon.com/apache2.0/

    or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package hf;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This sample shows how to create a simple speechlet for handling speechlet requests.
 */
public class HFSpeechlet implements Speechlet {


    private static final Logger log             = LoggerFactory.getLogger(HFSpeechlet.class);

    private static final String MANNSCHAFT_SLOT = "mannschaft";


    private Map<String, String> teamIds         = null;

    private Map<String, String> getTeamIds() {
        if (teamIds == null) {
            teamIds = new HashMap<String, String>();
            teamIds.put("1. herren", "2");
            teamIds.put("1 herren", "2");
            teamIds.put("1.herren", "2");
            teamIds.put("erste herren", "2");
            
            teamIds.put("2. herren", "9");
            teamIds.put("2 herren", "9");
            teamIds.put("2.herren", "9");
            teamIds.put("zweite herren", "9");
            
            teamIds.put("3. herren", "547");
            teamIds.put("3 herren", "547");
            teamIds.put("3.herren", "547");
            teamIds.put("dritte herren", "547");
            
            teamIds.put("a jugend", "4789");
            teamIds.put("a. jugend", "4789");
            teamIds.put("a.jugend", "4789");
            teamIds.put("a-jugend", "4789");
            
            teamIds.put("b jugend", "4827");
            teamIds.put("b. jugend", "4827");
            teamIds.put("b.jugend", "4827");
            teamIds.put("b-jugend", "4827");
            
            teamIds.put("c jugend", "4857");
            teamIds.put("c. jugend", "4857");
            teamIds.put("c.jugend", "4857");
            teamIds.put("c-jugend", "4857");
            
            teamIds.put("d jugend", "4901");
            teamIds.put("d. jugend", "4901");
            teamIds.put("d.jugend", "4901");
            teamIds.put("d-jugend", "4901");
        }
        return teamIds;
    }


    @Override
    public void onSessionStarted(final SessionStartedRequest request, final Session session) throws SpeechletException {

        log.info("onSessionStarted requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
        // any initialization logic goes here
    }

    @Override
    public SpeechletResponse onLaunch(final LaunchRequest request, final Session session) throws SpeechletException {
        log.info("onLaunch requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
        return getWelcomeResponse();
    }

    @Override
    public SpeechletResponse onIntent(final IntentRequest request, final Session session) throws SpeechletException {
        log.info("onIntent requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());

        Intent intent = request.getIntent();
        String intentName = (intent != null) ? intent.getName() : null;

        if ("GetTabellenplatzIntent".equals(intentName)) {
            return getTabellenplatzResponse(intent);
        } else if ("AMAZON.HelpIntent".equals(intentName)) {
            return getHelpResponse();
        } else {
            throw new SpeechletException("Invalid Intent");
        }
    }

    @Override
    public void onSessionEnded(final SessionEndedRequest request, final Session session) throws SpeechletException {
        log.info("onSessionEnded requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
        // any cleanup logic goes here
    }

    /**
     * Creates and returns a {@code SpeechletResponse} with a welcome message.
     *
     * @return SpeechletResponse spoken and visual response for the given intent
     */
    private SpeechletResponse getWelcomeResponse() {
        String speechText = "Willkommen zum Handballfreunde-Skill. Du kannst sagen, nach dem Tabellenplatz ";

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("HF");
        card.setContent(speechText);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        // Create reprompt
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }

    private String getTeamId(String mannschaft) {
        String ret = getTeamIds().get(mannschaft.toLowerCase());
        if (ret == null){
            ret = teamIds.get("1. herren");
        }
        return ret;
    }

    /**
     * Creates a {@code SpeechletResponse} for the hf intent.
     *
     * @return SpeechletResponse spoken and visual response for the given intent
     */
    private SpeechletResponse getTabellenplatzResponse(Intent intent) {


        Slot mannschaftSlot = intent.getSlot(MANNSCHAFT_SLOT);
        String speechText = "";
        String mannschaft = "";
        if (mannschaftSlot != null) {
            mannschaft = mannschaftSlot.getValue();
        } else {
            mannschaft = "1. herren";
        }

        System.out.println("Mannschaft: " + mannschaft);
        String teamId = getTeamId(mannschaft);

        System.out.println("Mannschaft-ID: " + teamId);
        System.out.println("Rufe aus...");

        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            HttpEntity<String> entity = new HttpEntity<String>(headers);
            String url = "http://handballfreunde05.com/rest/tabelle?team=" + teamId;
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

            System.out.println(response.getBody());

            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());

            String platz = root.findValue("Platzierung").textValue();
            String spiele = root.findValue("Anzahl der Spiele").textValue();
            String spieleGesamt = root.findValue("Anzahl der Spiele insgesamt").textValue();

            speechText =
                         "Die " + mannschaft + " der Handballfreunde belegt nach " + spiele + " von " + spieleGesamt + " Spielen den "
                             + platz + ". Rang.";


        } catch (Exception e) {
            System.out.println(e.toString());
        }

        System.out.println("Der Rueckgabetext ist:  " + speechText);

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Tabellenplatz");
        card.setContent(speechText);


        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        return SpeechletResponse.newTellResponse(speech, card);
    }

    /**
     * Creates a {@code SpeechletResponse} for the help intent.
     *
     * @return SpeechletResponse spoken and visual response for the given intent
     */
    private SpeechletResponse getHelpResponse() {
        String speechText = "Du könntest sagen, nach dem Tabellenplatz";

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("HF");
        card.setContent(speechText);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(speechText);

        // Create reprompt
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }
}
